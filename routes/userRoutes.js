// dependencies
const router = require('express').Router();

// imports
const User = require('../models/Users');
const userController = require('../controllers/userControllers');
const authen = require('../authen');

//// ROUTES
// register
router.post('/register', userController.checkUsernameExists, userController.checkEmailExists, userController.registerUser);

// login
router.post('/login', userController.loginUser);

// retrieve Authenticated User Cart
router.get('/cart', authen.verify, userController.getUserCart)

// update User role
router.patch('/update-role/:userId', authen.verify, userController.updateRole);

module.exports = router;