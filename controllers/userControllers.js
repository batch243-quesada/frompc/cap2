// dependencies
const bcrypt = require('bcrypt');

// import modules
const User = require('../models/Users');
const Cart = require('../models/Cart')
const authen = require('../authen')

// username
const checkUsernameExists = (request, response, next) => {
	return User.find({username: request.body.username})
	.then(result => {

		if(result.length > 0) response.send(`The username, ${request.body.username}, is already taken. Please use another username.`)

		else next();
	})
}

// email
const checkEmailExists = (request, response, next) => {
	return User.find({email: request.body.email})
	.then(result => {

		if(result.length > 0) response.send(`The email, ${request.body.email}, is already taken. Please use another email.`)

		else next();
	})
}

// register
const registerUser = (request, response) => {
	let newUser = new User(
			{
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				username: request.body.username,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNo: request.body.mobileNo
			}
		)

	return newUser
	.save()
	.then(user => {
		console.log(user);
		response.send(`Thank you, ${user.username}. You are now registered.`);
	})
	.catch(error => {
		console.log(error);
		response.send(`Sorry, there was an error during registration process. Please try again.`)
	})
}

// login
const loginUser = (request, response) => {
	return User.findOne({username: request.body.username})
	.then(result => {
		console.log(result);
		if(result === null) {
			response.send(`This username doesn't exist.`)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect) response.send({accessToken: authen.createAccessToken(result)});
			else response.send(`Incorrect password, please try again.`);
		}
	})
}

// modify User role
const updateRole = (request, response) => {
	const userData = authen.decode(request.headers.authorization);
	let userId = request.params.userId;

	if(userData.isAdmin) {
		return User.findById(userId)
		.then(result => {
			let update = {
				isAdmin : !result.isAdmin
			}

			return User.findByIdAndUpdate(userId, update, {new: true})
			.then(document => {
				document.password = "-------";
				console.log(document);
				response.send(document)
			})
			.catch(err => response.send(err));
		})
		.catch(err => response.send(err));
	} else response.send(`Access denied!`);
}

// get Authenticated User cart
const getUserCart = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		return Cart.find({id: userData.id})
		.then(result => response.send(result))
		.catch(err => {
			console.log(err);
			response.send(err);
		})
	} else response.send(`You don't have permission to do this action.`)
}

module.exports = {
	checkUsernameExists,
	checkEmailExists,
	registerUser,
	loginUser,
	updateRole,
	getUserCart
}